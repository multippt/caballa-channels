<?php
//$currentMirror = "local"; // local testing
$currentMirror = "openshiftws"; // remote deployment

$mirrors["local"] = array("http" => "http://localhost:5080", "https" => "https://localhost:5433");
$mirrors["ofmmo"] = array("http" => "http://www.ofmmo.com:5080", "https" => "https://www.ofmmo.com:5433");
$mirrors["heroku"] = array("http" => "http://caballachannels.herokuapp.com", "https" => "https://caballachannels.herokuapp.com");
$mirrors["openshift"] = array("http" => "http://channels-caballa.rhcloud.com", "https" => "https://channels-caballa.rhcloud.com");
$mirrors["openshiftws"] = array("http" => "http://channels-caballa.rhcloud.com:8000", "https" => "https://channels-caballa.rhcloud.com:8443");
$mirrors["appfog"] = array("http" => "http://caballachannels.ap01.aws.af.cm", "https" => "https://caballachannels.ap01.aws.af.cm");

// Returns preferred mirror
function getMirror() {
	global $mirrors, $currentMirror;
	return json_encode($mirrors[$currentMirror]);
}
?>