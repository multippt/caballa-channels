function ChannelClient($socket) {
	this.socket = $socket;
	this.state = "";
	
	this.alias = "";
	this.id = "";
	this.channel = "";
	this.lastMessage = null;
	this.allowRetry = false;
	
	// events
	this.onchangestate = function(state) { };
	this.onreceive = function(data) { };
	this.list = function(data) { };
	
}
// Change current state
ChannelClient.prototype.setState = function(newState) {
	this.state = newState;
	this.onchangestate(newState);
}
// Register connection events
// Call init once events are set
ChannelClient.prototype.init = function() {
	var client = this;
	this.socket.on("connect", function() {
		client.setState("connected");
	});
	this.socket.on("error", function(data) {
		client.setState("connectfail");
	});
	this.socket.on("connect_failed", function() {
		client.setState("connectfail");
	});
	this.socket.on("reconnect_failed", function() {
		client.setState("connectfail");
	});
	this.socket.on("reconnecting", function() {
		client.setState("reconnecting");
	});
	this.socket.on("reconnect", function() {
		client.setState("connected");
	});
	
	this.initChannel();
}
// Register channel events
ChannelClient.prototype.initChannel = function() {
	var client = this;
	this.socket.on("identity", function(data) {
		client.id = data.id;
	});
	
	this.socket.on("joined", function(data) {
		var channel = data;
		client.channel = channel;
		client.socket.removeAllListeners(channel);
		client.socket.on(channel, client.onreceive);
		client.setState("group");
	});
	this.socket.on("identify", function() {
		client.setState("askgroup");
		client.retry();
	});
	this.socket.on("list", function(data) {
		client.list(data);
	});
	this.socket.on("info", function(data) {
		console.log(data);
	});
	this.socket.on("grouperror", function(data) {
		console.log(data);
		if (data.type == "noid") {
			if (client.alias == "") {
				client.setState("askname");
			} else {
				client.socket.emit("identify", client.alias);
				client.retrying = true;
			}
		}
		if (data.type == "nogroup") {
			console.log(data.message);
			client.setState("askgroup");
		}
	});	
}
// Resend the last message
ChannelClient.prototype.retry = function() {
	if (this.allowRetry && this.lastMessage !== null) {
		this.socket.emit(this.lastMessage.type, this.lastMessage.data);
	}
}
// Send message to this channel
ChannelClient.prototype.send = function(message) {
	if (this.channel !== null) {
		this.socket.emit(this.channel, message);
	}
}
// Set alias of user
ChannelClient.prototype.identify = function(id) {
	if (id !== "") {
		this.alias = id;
		this.socket.emit("identify", this.alias);
	}
}
// Refresh channel list
ChannelClient.prototype.refresh = function() {
	this.socket.emit("list");
}
// Join a channel
ChannelClient.prototype.join = function(channelid) {
	this.lastMessage = {type: "join", data: channelid};
	this.socket.emit("join", channelid);
}
// Leave a channel
ChannelClient.prototype.leave = function() {
	if (this.channel !== "") {
		this.socket.emit("leave", this.channel);
		this.socket.removeAllListeners(this.channel);
		this.channel = "";
		this.setState("askgroup");
	}
}
// Create a channel
ChannelClient.prototype.create = function() {
	this.lastMessage = {type: "create", data: null};
	this.socket.emit("create");
}
