<?php
try {
	include_once("config.php");
	include_once("sdk/facebook.php");
} catch (Exception $e) {
	echo $e->getMessage();
}

$facebook = new Facebook( $config );
$uid = $facebook->getUser();
$urlparams = array();

if ($uid == 0) {
	$islogin = false;
	$loginurl = $facebook->getLoginUrl($urlparams);
	//echo "<p><a href=\"".$facebook->getLoginUrl($urlparams)."\">Login</a></p>";
	include("client.php");
} else {
	$useralias = getUserName();
	$islogin = true;
	$logouturl = $facebook->getLogoutUrl();
	include("client.php");
	
	//echo "<p><a href=\"".$facebook->getLogoutUrl()."\">Logout</a></p>";
}

function getUserName() {
	global $facebook;
	$profile = $facebook->api("/me", "GET");
	return $profile["name"];
}
?>