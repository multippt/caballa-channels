<!DOCTYPE html>
<html>
<head>
<title>Channels</title>
<script src="./js/jquery_1.10.2.js"></script>
<script src="./js/socket.io.js"></script>
<script src="channelclient.js"></script>
<script>
	var socket;
	var server = <?php 
		include("mirror.php");
		echo getMirror(); 
	?>;
	if (location.protocol === "https:") {
		console.log("Connecting to " + server["https"]);
		socket = io.connect(server["https"], { "secure": true });
	} else {
		console.log("Connecting to " + server["http"]);
		socket = io.connect(server["http"]);
	}
	
	<?php
		$alias = "";
		if (isset($_GET["alias"])) {
			$alias = $_GET["alias"];
		}
		if (isset($useralias)) {
			$alias = $useralias;
		}
	?>
	var alias = "<?php echo $alias; ?>";
</script>
<script src="newclient.js"></script>
<script>client.identify(alias);</script>
<link href="channels.css" rel="stylesheet" type="text/css">

</head>


<body>
<div class="container box">
  <div class="menu">
    <div class="tabcontainer">
      <div class="tab round selected"> <a href="javascript:home();">Home</a> </div>
	  <?php if (isset($islogin) && $islogin) { ?>
	  <div class="tab round loginout"> <a href="<?php echo $logouturl; ?>">Logout</a> </div>
	  <?php } else if (isset($islogin) && !$islogin) { ?>
	  <div class="tab round loginout"> <a href="<?php echo $loginurl; ?>">Login</a> </div>
	  <?php } ?>
      <!--<div class="tab round"> <a href="#">Channel Name</a>
        <div class="close"><a href="javascript:leave();">x</a></div>
      </div>-->
      <div class="fill">&nbsp;</div>
    </div>
  </div>
  <div class="filler">
	<div class="mid-panel connecting-panel">
		<div id="connecting-panel" class="panel connecting-panel">
			Connecting to server...
		</div>
	</div>
	<div class="mid-panel reconnecting-panel connectfail-panel askname-panel">
		<!-- substitute panels -->
		<div id="reconnecting-panel" class="panel hidden reconnecting-panel">
			Connecting to server...
		</div>
		<div id="connectfail-panel" class="panel hidden connectfail-panel">
			Unable to connect to server. Please try again later.
		</div>
		<div id="askname-panel" class="panel hidden askname-panel">
			<p>Please state your name</p>
			<div class="submit-panel">
				<input type="text" onkeyup="submit(event,'#btn_setid')" id="myid" value="test">
				<input type="button" id="btn_setid" onclick="javascript:setID()" value="Go!">
			</div>
			
			<div style="margin-top: 10px;font-size: 0.9em;color:#999;"><a href="..">What is Channels?</a></div>
		</div>
		<!-- end substitutes -->
	</div>
  
    <div class="left-panel box askgroup-panel group-panel">
	
	<!-- Chat listings -->
	<div id="askgroup-panel" class="askgroup-panel">
      <!--<div class="search-panel">
        <input class="search round box" type="text">
      </div>-->
      <div class="chatmenu">
        <div class="tabcontainer">
          <div class="tab selected round-top"> All Channels </div>
          <!--<div class="tab round-top"> Bookmarked </div>-->
          <div class="fill">&nbsp;</div>
        </div>
      </div>
      <div class="chatlistcontainer round box">
        <div class="chatlist">
          <div class="list" id="groupList">

          </div>
        </div>
      </div>
	</div>
	<!-- End Chat listings -->
	
	<!-- Chat panel -->
	<div id="group-panel" class="group-panel">
	<div class="chatcontainer round box">
        <div class="chatwindow">
          <div class="list" id="chatdisplay">
            <div class="item">
              <div class="date">26 minutes ago</div>
              <div class="name">Person 1:</div>
              <div class="message">Chat message</div>
            </div>
            <div class="item">
              <div class="date">23 minutes ago</div>
              <div class="name">Person 2:</div>
              <div class="message">Chat message 2</div>
            </div>
          </div>
        </div>
      </div>
      <div class="chatinputcontainer round box">
        <div class="chatinputcontent">
          <div>
            <textarea type="text" class="chatinput box" id="chatid" onkeyup="submit(event,'#btn_chat')" id="chatbox"></textarea>
          </div>
          <div class="button box round" id="btn_chat" onclick="javascript:startChat();">Send</div>
        </div>
      </div>
	</div>
	<!-- Chat panel -->
    </div>
    <div class="right-panel box askgroup-panel group-panel">
      <div class="panel round box">
        <div class="profile">
          <!--<div class="editbutton round">Edit</div>-->
          <div class="image">
            <div class="imageplaceholder">&nbsp;</div>
          </div>
          <div class="name" id="profile-name"><?php echo $alias; ?></div>
        </div>
        <div class="buttonmenu askgroup-panel">
          <div class="button box round" onclick="javascript:create();">Create</div>
          <!--<div class="button box round" onclick="javascript:join();">Join</div>-->
        </div>
		<div class="buttonmenu group-panel">
          <div class="button box round" onclick="javascript:leave();">Leave</div>
        </div>
      </div>
    </div>
    <div class="fill">&nbsp;</div>
  </div>
</div>
</body>
</html>