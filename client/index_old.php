<!DOCTYPE html>
<html>
    <head>
        <title>Chat room</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
		<script src="./js/jquery_1.10.2.js"></script>
		<script src="./js/socket.io.js"></script>
		<script src="channelclient.js"></script>
		<link rel="stylesheet" href="style.css">
    </head>
    <body>
	
	<script>
		var socket;
		var server = <?php 
			include("mirror.php");
			echo getMirror(); 
		?>;
		if (location.protocol === "https:") {
			console.log("Connecting to " + server["https"]);
			socket = io.connect(server["https"], { "secure": true });
		} else {
			console.log("Connecting to " + server["http"]);
			socket = io.connect(server["http"]);
		}
	</script>
	<script src="client.js"></script>
	<script>
		<?php
			$alias = "";
			if (isset($_GET["alias"])) {
				$alias = $_GET["alias"];
			}
			if (isset($useralias)) {
				$alias = $useralias;
			}
		?>
		var alias = "<?php echo $alias; ?>";
		client.identify(alias);
	</script>
	
	<div id="connecting-panel" class="panel">
		Connecting to server...
	</div>
	<div id="reconnecting-panel" class="panel hidden">
		Connecting to server...
	</div>
	<div id="connectfail-panel" class="panel hidden">
		Unable to connect to server. Please try again later.
	</div>
	<div id="askname-panel" class="panel hidden">
		<p>Please state your name</p>
		<div class="submit-panel">
			<input type="text" onkeyup="submit(event,'#btn_setid')" id="myid" value="test">
			<input type="button" id="btn_setid" onclick="javascript:setID()" value="Go!">
		</div>
		
		<div style="margin-top: 10px;font-size: 0.9em;color:#999;"><a href="..">What is Channels?</a></div>
	</div>
	<div id="askgroup-panel" class="panel hidden">
		<p>Join a group</p>
		<div class="submit-panel">
			<input type="button" onclick="javascript:create();" value="Create">
			<input type="button" onclick="javascript:refresh();" value="Refresh">
		</div>
		<div id="groupList"></div>
	</div>
	<div id="group-panel" class="panel hidden">
		<div id="chatdisplay"></div>
		<div class="submit-panel">
			<table border="0" cellspacing="0" cellpadding="0">
			<tr><td width="100%">
			<input type="text" onkeyup="submit(event,'#btn_chat')" id="chatid" value="">
			</td><td>
			<input type="button" id="btn_chat" onclick="javascript:startChat()" value="Chat">
			</td>
			</table>
			<input type="button" onclick="javascript:leave();" value="Leave group">
		</div>
	</p>
	</div>
    </body>
</html>
