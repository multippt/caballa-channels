
var client = new ChannelClient(socket);

var panels = { };
panels["connecting"] = "#connecting-panel";
panels["reconnecting"] = "#reconnecting-panel";
panels["connectfail"] = "#connectfail-panel";
panels["askname"] = "#askname-panel";
panels["askgroup"] = "#askgroup-panel";
panels["group"] = "#group-panel";

var lastState = "";
// Called on state change
function stateChange(state) {
	if (lastState != state) {
		// Update state
		stateChangeTrigger(state); // intercept states and change state
		lastState = client.state;
		state = client.state;
		
		// Hide/show related panels
		for (var panel in panels) {
			if (panel === state) {
				$(panels[panel]).show();
			} else {
				$(panels[panel]).hide();
			}
		}
	}
}

// Intercept states and transit to other states
function stateChangeTrigger(state) {
	if (state == "connected") {
		if (client.alias == "") {
			client.state = "askname";
		} else {
			client.state = "askgroup";
			refresh();
		}
		return;
	}
	if (state == "group") {
		clearChat();
		return;
	}
	if (state == "askgroup") {
		refresh();
	}
}

// Called when data received
function onreceive(data) {
	var chatdisp = $("#chatdisplay").get(0);
	if (data.type == "chat") {
		var output = "<b>" + data.message.alias + "</b>: " + data.message.data + "<br>";
		chatdisp.innerHTML += output;
	}
	if (data.type == "info") {
		var output = data.message + "<br>";
		chatdisp.innerHTML += output;
	}
	chatdisp.scrollTop = chatdisp.scrollHeight;
}

// Called when listing groups
function list(groups) {
	var glist = $("#groupList").get(0);
	var result = "";
	for (var group in groups) {
		var groupname = groups[group].name;
		result += "<a href=\"javascript:join(\'" + groups[group].id +"\');\">" + groupname + "</a><br>";
	}
	glist.innerHTML = result;
}

client.onchangestate = stateChange;
client.onreceive = onreceive;
client.list = list;
client.init(); // register socket events

// UI wrapper for client
function clearChat() {
	var chatdisp = $("#chatdisplay").get(0);
	chatdisp.innerHTML = "";
}
function startChat() {
	var message = $("#chatid").get(0).value;
	if (message !== "") {
		client.send(message);
	}
	$("#chatid").get(0).value = "";
}
function setID() {
	client.identify($("#myid").get(0).value);
}
function refresh() {
	client.refresh();
}
function join(gid) {
	client.join(gid);
}
function leave() {
	client.leave();
}
function submit(e, objname) {
	if (e.keyCode == 13) {
		var button = $(objname).get(0);
		button.onclick();
	}
}
function create() {
	client.create();
}