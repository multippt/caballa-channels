require("./channelmanager.js");
var manager = new ChannelManager();
manager.useridgen = 0;

function start(io) {
	io.sockets.on("connection", function(socket) {
		var uid =  manager.useridgen++;
		var ualias = null;
		socket.emit("identity", {id: uid});
		
		socket.on("disconnect", function() {
			if (uid !== null) {
				manager.leaveall(uid);
			}
		});
		socket.on("identify", function(data) {
			if (ualias === null) {
				ualias = data;
				if (ualias !== null) {
					manager.addUser(uid, ualias, socket);
					socket.emit("identify");
				} else {
					socket.emit("grouperror", {type: "badid", message: "Cannot use this id" });
				}
			} else {
				socket.emit("grouperror", {type: "badid", message: "Already identified" });
			}
		});
		socket.on("list", function() {
			socket.emit("list", manager.list());
		});
		socket.on("join", function(data) {
			if (ualias !== null) {
				manager.join(uid, data);
			} else {
				socket.emit("grouperror", {type: "noid", message: "Please identify first" });
			}
		});
		socket.on("create", function() {
			if (ualias !== null) {
				socket.emit("create", manager.create(uid));
			} else {
				socket.emit("grouperror", {type: "noid", message: "Please identify first" });
			}
		});
		socket.on("leave", function(data) {
			if (ualias !== null) {
				manager.leave(uid, data);
			} else {
				socket.emit("grouperror", {type: "noid", message: "Please identify first" });
			}
		});
		socket.on("identity", function() {
			socket.emit("identity", {id: uid});
		});
	});
}

exports.start = start;