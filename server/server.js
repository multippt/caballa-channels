// Entry point for local server
// Configure ports for relevant environments

// Default ports
var normalport = 5080;
var secureport = 5433;
var hostname = "0.0.0.0";

var socketio = require("socket.io");
var start = require("./serverinit").start;

if (typeof(deploy) === "undefined") {
	global.deploy = true;
}

if (!deploy) {
	// Listen on HTTPS
	var fs = require("fs");
	var options = {
		key: fs.readFileSync("./certs/server.key"),
		cert: fs.readFileSync("./certs/server.cert"),
		requestCert: true,
		rejectUnauthorized: false
	};
	var https = require("https");
	https.globalAgent.options.rejectUnauthorized = false;
	var httpsServer = https.createServer(options).listen(secureport);
	var secureio = socketio.listen(httpsServer);
	start(secureio);
	
	console.log("Secure channel on port " + secureport);
}

if (deploy) {
	// Supports: heroku, openshift, appfrog
	// Fallback to default if none are applicable
	hostname = 
		process.env.OPENSHIFT_NODEJS_IP || 
		hostname;
	normalport = 
		process.env.VMC_APP_PORT || 
		process.env.OPENSHIFT_INTERNAL_PORT || 
		process.env.OPENSHIFT_NODEJS_PORT || 
		process.env.PORT || 
		normalport;
}

// Listen on HTTP
var http = require("http");
var httpServer = http.createServer().listen(normalport, hostname);
var normalio = socketio.listen(httpServer);
console.log("Normal channel on port " + normalport);

if (deploy) {
	
	if (typeof(process.env.OPENSHIFT_NODEJS_PORT) !== "undefined") {
		// openshift supports websockets
	} else {
		// force long polling
		normalio.configure(function () { 
			normalio.set("transports", ["xhr-polling"]); 
			normalio.set("polling duration", 10); 
		});
	}
}

start(normalio);