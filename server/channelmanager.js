require("./channels.js");

/** Channel management **/
function ChannelManager() {
	this.channels = new Channels();
	this.users = { };
}
ChannelManager.prototype.addUser = function(uid, ualias, socket) {
	this.users[uid] = {id: uid, alias: ualias, socket: socket, channels: { }};
};
ChannelManager.prototype.broadcast = function(channelid, message) {
	var channel = this.channels.get(channelid);
	for(var user in channel.users) {
		channel.users[user].socket.emit(channelid, message);
	}
};
ChannelManager.prototype.join = function(uid, channelid) {
	var channel = this.channels.get(channelid);
	if (channel !== null) {
		var result = channel.add(this.users[uid]);
		this.users[uid].channels[channelid] = channel;

		this.users[uid].socket.removeAllListeners(channelid); // purge duplicates
		var manager = this;
		this.users[uid].socket.on(channelid, function(data) {
			// Implement chatting system
			var chatmsg = {uid: uid, alias: manager.users[uid].alias, data: data};
			manager.broadcast(channelid, {type: "chat", message: chatmsg});
		});

		this.users[uid].socket.emit("joined", channelid);
		//this.users[uid].socket.emit("info", "Join result " + result);
		this.broadcast(channelid, {type: "info", message: this.users[uid].alias + " has joined " + channel.name});
		
		// Notify all
		this.broadcastall("list", this.list());
	} else {
		this.users[uid].socket.emit("grouperror", {type: "nogroup", message: "No such channel"});
	}
};
ChannelManager.prototype.leave = function(uid, channelid) {
    var channel = this.channels.get(channelid);
	
	if (channel !== null) {
		channel.remove(this.users[uid]);
		delete this.users[uid].channels[channelid];

		this.users[uid].socket.removeAllListeners(channelid);
		
		this.broadcast(channelid, {type: "info", message: this.users[uid].alias + " has left"});
		
		if (channel.count === 0) {
			this.channels.disband(channel.id);
		}
		
		// Notify all
		this.broadcastall("list", this.list());
	} else {
		this.users[uid].socket.emit("grouperror", {type: "nogroup", message: "No such channel"});
	}
};
ChannelManager.prototype.leaveall = function(uid) {
    var user = this.users[uid];
	if (typeof(user) !== "undefined") {
		for (var channel in user.channels) {
			this.leave(uid, user.channels[channel].id);
		}
	}
};
ChannelManager.prototype.list = function() {
	var result = { };
	for (var channel in this.channels.channels) {
		var users = { };
		for (var user in this.channels.channels[channel].users) {
			var userslist = this.channels.channels[channel].users;
			users[user] = {id: userslist[user].id, alias: userslist[user].alias};
		}
		result[channel] = {
			id: this.channels.channels[channel].id,
			name: this.channels.channels[channel].name,
			users: users
		};
	}
	return result;
};
ChannelManager.prototype.create = function(uid) {
	var channel = this.channels.create();
	this.channels.get(channel).name = "Channel " + channel;
	this.join(uid, channel);
	return channel;
};
// Broadcast to all users using the channel parameter
ChannelManager.prototype.broadcastall = function(channelid, message) {
	for (var user in this.users) {
		this.users[user].socket.emit(channelid, message);
	}
}

global.ChannelManager = ChannelManager;