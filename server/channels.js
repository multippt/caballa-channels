/** Channels 
 *  A collection of channels
 * **/
function Channels() {
    this.channels = { };
    this.templates = { };
    this.templates["default"] = function(channel) { return channel; };
}
// Create a channel using template
Channels.prototype.create = function(template) {
    if (template == null) {
        template = "default";
    }
    if (typeof(this.templates[template]) !== "undefined") {
        var channel = new Channel();
        this.channels[channel.id] = channel;
        this.templates[template](channel); // pass channel through template
        return channel.id;
    }
    return null;
};
// Deletes the channel
Channels.prototype.disband = function(channelid) {
    if (typeof(this.channels[channelid]) !== "undefined") {
        delete this.channels[channelid];
    }
}
// Adds a channel template
Channels.prototype.register = function(template, callback) {
    this.templates[template] = callback;
};
// Remove a channel template
Channels.prototype.unregister = function(template) {
    if (typeof(this.templates[template]) !== "undefined") {
        delete this.templates[template];
    }
};
// Add a user to channel
Channels.prototype.add = function(channelid, user) {
    var channel = this.get(channelid);
    if (channel !== null) {
	return channel.add(user);
    }
    return 0;
};
// Remove a user from channel
Channels.prototype.remove = function(channelid, user) {
    var channel = this.get(channelid);
    if (channel !== null) {
	return channel.remove(user);
    }
    return 0;
};
Channels.prototype.get = function(channelid) {
    if (typeof(this.channels[channelid]) !== "undefined") {
        return this.channels[channelid];
    }
    return null;
};

/** Channel 
 *  A collection of users
 * **/
function Channel() {
    this.id = Channel.idgenerator++;
    this.users = { };
    this.count = 0;
    var channel = this;
    this.add = function(user) {
		if (typeof(channel.users[user.id]) === "undefined") {
			channel.users[user.id] = user;
			channel.count++;
			return 1;
		} else {
			channel.users[user.id] = user;
			return 2; // repeated user
		}
    };
    this.remove = function(user) {
		delete channel.users[user.id];
		channel.count--;
		return 1;
    }
}
Channel.idgenerator = 0;


global.Channels = Channels;
global.Channel = Channel;